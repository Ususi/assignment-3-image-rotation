#include "../include/target/formats/bmp/from_bmp.h"
#include "../include/target/formats/bmp/to_bmp.h"
#include "../include/target/functionality/file/file.h"

static int error_exit(const char *const message) {
    fprintf(stderr, "%s", message);
    return 1;
}

int execute_rotating_scenario(int argc, char** argv) {
    if (argc != 3) {
        return error_exit("Program executes with 2 parameters.");
    }

    struct image first_image = {0};
    struct image second_image = {0};


    enum read_status first_image_read_status;
    {
        enum open_status file_opened;
        enum read_status image_read;
        enum close_status file_closed;
        FILE* first_file = NULL;
        const char * const first_file_name = argv[1];

        file_opened = open_file(&first_file, first_file_name, "rb");
        if (file_opened == OPEN_FAILED_NO_SUCH_FILE) {
            return READ_FAILED_OPEN;
        }
        image_read = from_bmp(first_file, &first_image);
        if (image_read != READ_OK) {
            return image_read;
        }
        file_closed = close_file(&first_file);
        first_image_read_status = file_closed == CLOSE_FAILED ? READ_FAILED_CLOSE : READ_OK;
    }

    if (first_image_read_status != READ_OK) {

        return error_exit("Program failed while reading input file.");
    }

    second_image = rotate(&first_image);
    delete_image(&first_image);

    enum write_status second_image_write_status;
    {
        enum open_status file_opened;
        enum write_status image_written;
        enum close_status file_closed;
        FILE* second_file = NULL;
        const char * const second_file_name = argv[2];

        file_opened = open_file(&second_file, second_file_name, "wb");
        if (file_opened == OPEN_FAILED_NO_SUCH_FILE) {
            return WRITE_FAILED_OPEN;
        }
        image_written = to_bmp(second_file, &second_image);
        if (image_written != WRITE_OK) {
            return image_written;
        }

        file_closed = close_file(&second_file);
        second_image_write_status = file_closed == CLOSE_FAILED ? WRITE_FAILED_CLOSE : WRITE_OK;
    }

    delete_image(&second_image);

    if (second_image_write_status != WRITE_OK) {
        return error_exit("Program failed while writing output file.");
    }

    return 0;
}



