#include "../../../include/target/functionality/image/image.h"


struct image initialize_image(const size_t width, const size_t height) {
    struct pixel *pixels = malloc(width * height * sizeof(struct pixel));
    struct image image =  {
            .width = width,
            .height = height,
            .data = pixels
    };
    return image;
}

void delete_image(struct image *const image) {
    if (image != NULL) {
        free(image -> data);
    }
}

uint8_t count_padding(const size_t width) {
    uint8_t padding_size = width * sizeof(struct pixel) % 4;
    return padding_size != 0 ? 4 - padding_size : padding_size;
}

struct pixel get_pixel(const struct image *const image, size_t row, size_t column) {
    return image -> data[image->width * row + column];
}

void set_pixel(struct image * const image, size_t row, size_t column, const struct pixel pixel) {
    image -> data[image->width * row + column] = pixel;
}

struct image rotate(struct image *const image) {
    if (image == NULL) return (struct image) {0};

    size_t width = image->width;
    size_t height = image->height;

    struct image new_image = initialize_image(height, width);
    if (new_image.data) {
        for (size_t i = 0; i < height; i++) {
            for (size_t j = 0; j < width; j++) {
                set_pixel(&new_image, j,  height - i - 1, get_pixel(image, i, j));
            }
        }
        return new_image;
    }
    else {
        return (struct image) {0};
    }
}

