#include "../../../../../include/target/formats/bmp/to_bmp.h"

static enum write_status write_header_to_file(FILE * const file, struct bmp_header * const file_header) {
    return fwrite(file_header, sizeof(struct bmp_header), 1, file) != 1 ? WRITE_FAILED_HEADER : WRITE_OK;
}

static enum write_status write_pixels_to_file(FILE * const file, const struct image * const image) {
    size_t width = image -> width;
    size_t height = image -> height;
    struct pixel* image_data_ptr = image -> data;

    uint8_t padding_byte_size = count_padding(width);
    uint64_t padding_value = 0;

    for (size_t i = 0; i < height; i++) {
        if (fwrite(image_data_ptr + i * width, sizeof(struct pixel), width, file) != width) {
            return WRITE_FAILED_PIXELS;
        }
        if (fwrite(&padding_value, 1, padding_byte_size, file)!= padding_byte_size) {
            return WRITE_FAILED_PIXELS;
        }
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE * const file, const struct image * const image) {
    if (file == NULL) return WRITE_FAILED_OPEN;
    if (image == NULL) return WRITE_FAILED_PIXELS;
    struct bmp_header file_header = create_bmp_header(image);
    return write_header_to_file(file, &file_header) == WRITE_OK ? write_pixels_to_file(file, image) : WRITE_FAILED_HEADER;
}


