#include "../../../../../include/target/formats/bmp/from_bmp.h"

static enum read_status read_header_from_file(FILE * const file, struct bmp_header * const file_header) {
    return fread(file_header, sizeof(struct bmp_header), 1, file) != 1 ?
           READ_FAILED_INVALID_HEADER_FORMAT : READ_OK;
}

static enum read_status read_pixels_from_file(FILE * const file, const struct image * const image) {
    size_t width = image -> width;
    size_t height = image -> height;
    struct pixel* pixels = image -> data;
    uint8_t padding_size = count_padding(width);

    for (size_t i = 0; i < height; i++) {
        size_t pixels_in_row;
        size_t seek_padding;

        pixels_in_row = fread(pixels + i * width, sizeof(struct pixel), width, file);
        if (pixels_in_row != width) {
            return READ_FAILED_INVALID_FILE;
        }
        seek_padding = fseek(file, padding_size, SEEK_CUR);
        if (seek_padding) {
            return READ_FAILED_INVALID_PIXEL_DATA;
        }
    }
    return READ_OK;
}

enum read_status from_bmp(FILE * const file, struct image * const image) {
    if (file == NULL) return READ_FAILED_OPEN;
    struct bmp_header file_header = {0};
    enum read_status header_read;
    enum read_status pixels_read;

    header_read = read_header_from_file(file, &file_header);
    if (header_read != READ_OK) {
        return header_read;
    }

    *image = initialize_image(file_header.biWidth, file_header.biHeight);

    pixels_read = read_pixels_from_file(file, image);
    if (pixels_read != READ_OK) {
        delete_image(image);
    }
    return pixels_read;
}


