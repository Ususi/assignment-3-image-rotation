#include "../../../../../include/target/formats/bmp/bmp.h"

#define BMP_TYPE 19778
#define BMP_PLANES 1
#define BMP_USED_COLOR 0
#define BMP_COMPRESSION 0
#define BMP_COLOR_DEPTH 24
#define BMP_INFO 40
#define BMP_PIXELS_PER_ONE 0
#define BMP_IMPORTANT 0
#define BMP_RESERVED 0


struct bmp_header create_bmp_header(const struct image *const image) {
    size_t width = image->width;
    size_t height = image->height;
    uint32_t image_byte_size = (sizeof(struct pixel) * width + count_padding(width)) * height;
    uint32_t bmp_header_byte_size = sizeof(struct bmp_header);
    return (struct bmp_header) {
            .bfType = BMP_TYPE,
            .bfileSize = bmp_header_byte_size + image_byte_size,
            .bOffBits = bmp_header_byte_size,
            .biSizeImage = image_byte_size,
            .bfReserved = BMP_RESERVED,
            .biCompression = BMP_COMPRESSION,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = BMP_PLANES,
            .biSize = BMP_INFO,
            .biBitCount = BMP_COLOR_DEPTH,
            .biXPelsPerMeter = BMP_PIXELS_PER_ONE,
            .biYPelsPerMeter = BMP_PIXELS_PER_ONE,
            .biClrUsed = BMP_USED_COLOR,
            .biClrImportant = BMP_IMPORTANT
    };
}

