#include "../../../include/target/functionality/file/file.h"

enum open_status open_file(FILE **file, const char *filename, const char * const mode) {
    *file = fopen(filename, mode);
    return (*file != NULL) ? OPEN_OK : OPEN_FAILED_NO_SUCH_FILE;
}

enum close_status close_file(FILE **file) {
    return fclose(*file) ? CLOSE_FAILED : CLOSE_OK;
}
