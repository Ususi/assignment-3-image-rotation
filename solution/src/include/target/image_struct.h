#ifndef IMAGE_STRUCT_H
#define IMAGE_STRUCT_H

#include <stddef.h>
#include <stdint.h>

struct pixel {
    uint8_t b,r,g;
};

struct image {
    size_t width;
    size_t height;
    struct pixel* data;
};

#endif
