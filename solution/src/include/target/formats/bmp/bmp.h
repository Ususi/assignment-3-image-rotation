#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "../../../target/formats/bmp/bmp_struct.h"
#include "../../../target/functionality/image/image.h"
#include "../../../target/image_struct.h"

struct bmp_header create_bmp_header(const struct image * const image);

#endif
