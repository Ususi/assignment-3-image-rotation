#ifndef IMAGE_TRANSFORMER_FROM_BMP_H
#define IMAGE_TRANSFORMER_FROM_BMP_H

#include "../../../target/formats/bmp/bmp.h"
#include "../../../target/functionality/image/image.h"
#include "../../../target/image_struct.h"
#include "../../../util/image_interaction_status.h"

#include <stdio.h>

enum read_status from_bmp(FILE* file, struct image* image);


#endif //IMAGE_TRANSFORMER_FROM_BMP_H
