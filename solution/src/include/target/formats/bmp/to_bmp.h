#ifndef IMAGE_TRANSFORMER_TO_BMP_H
#define IMAGE_TRANSFORMER_TO_BMP_H

#include "../../../target/formats/bmp/bmp.h"
#include "../../../target/functionality/image/image.h"
#include "../../../target/image_struct.h"
#include "../../../util/image_interaction_status.h"

#include <stdio.h>

enum write_status to_bmp(FILE* file, const struct image* image);

#endif //IMAGE_TRANSFORMER_TO_BMP_H
