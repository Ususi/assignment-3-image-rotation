#ifndef IMAGE_TRANSFORMER_FILE_H
#define IMAGE_TRANSFORMER_FILE_H

#include "../../../util/file_interaction_status.h"
#include <stdio.h>

enum open_status open_file(FILE **file, const char *filename, const char * const mode);

enum close_status close_file(FILE **file);

#endif //IMAGE_TRANSFORMER_FILE_H
