#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include "../../../target/image_struct.h"
#include <stdint.h>
#include <stdlib.h>

struct image initialize_image(size_t width, size_t height);

void delete_image(struct image *image);

struct pixel get_pixel(const struct image *image, size_t row, size_t column);

void set_pixel(struct image * image, size_t row, size_t column, struct pixel pixel);

uint8_t count_padding(size_t width);

struct image rotate(struct image * image);


#endif
